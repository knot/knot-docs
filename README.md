# Knot Docs

This repo contains scripts to generate and manage a simple hub page with links
to various versions of the Knot Resolver documentation (and maybe docs for other
projects in the future).

It serves as a piece of the puzzle to replace
[ReadTheDocs](https://about.readthedocs.com/) with something we control
completely, including the container build images.


## Pre-requisites

You need to have Python >=3.8 and Poetry installed.


## Usage

```
$ poetry env use $(which python3)
$ poetry install
$ poetry run ./gen-pages.py \
    --gitlab <gitlab URL> \
    --token <security token> \
    --project <project ID>
```
