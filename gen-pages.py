#!/usr/bin/env python3

import argparse
import jinja2
import json
import os
import sys
import urllib

from datetime import datetime
from http.client import HTTPConnection, HTTPSConnection, HTTPResponse
from pathlib import Path
from typing import Dict, Optional, Union


j2env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(""),
    autoescape=jinja2.select_autoescape(["html"]),
)

j2t = j2env.get_template("template.html")


class GitLabConn:
    """Represents a connection to GitLab API."""

    API_ROOT = "/api/v4"

    def __init__(self, root_url: str, project_id: str, token: str):
        url = urllib.parse.urlparse(root_url)
        self.conn: Union[HTTPConnection, HTTPSConnection]
        if url.scheme == "http":
            self.conn = HTTPConnection(url.hostname, url.port)
        elif url.scheme == "https":
            self.conn = HTTPSConnection(url.hostname, url.port)
        else:
            raise ValueError(f"Invalid URL scheme '{url.scheme}'")
        self.root_path = url.path
        self.project_id = project_id
        self.token = token

    def request_np(
        self,
        method: str,
        endpoint: str,
        body: Optional[str] = None,
        headers: Optional[Dict[str, str]] = None,
    ) -> HTTPResponse:
        """Sends a request to GitLab outside of a project."""

        path = f"{self.root_path}/api/v4/{endpoint}"
        if headers is None:
            headers = {}
        headers["Authorization"] = f"Bearer {self.token}"
        self.conn.request(method, path, body, headers)
        return self.conn.getresponse()


    def request(
        self,
        method: str,
        endpoint: str,
        body: Optional[str] = None,
        headers: Optional[Dict[str, str]] = None,
    ) -> HTTPResponse:
        """Sends a request to GitLab inside of the configured project."""

        return self.request_np(
            method,
            f"projects/{self.project_id}/{endpoint}",
            headers,
            body
        )


def generate(ns: argparse.Namespace):
    conn = GitLabConn(ns.gitlab, ns.project, ns.token)

    develop_envs = []
    release_envs = []
    next_page = "1"

    while next_page is not None:
        print(f"Requesting page {next_page} ...")
        response = conn.request("GET", f"environments?page={next_page}&states=available")
        if response.status != 200:
            raise ValueError(f"Server returned {response.status}")

        envs_bytes = response.read()
        envs = json.loads(envs_bytes.decode("utf-8"))

        for e in envs:
            if "external_url" not in e or e["external_url"] is None:
                continue
            name = e["name"]
            try:
                ix = name.find("/")
                folder = name[0:ix]
                e["xname"] = name[ix+1:]
                if folder == "docs-develop":
                    develop_envs.append(e)
                    print(f"Added {e['xname']} to develop")
                elif folder == "docs-release":
                    release_envs.append(e)
                    print(f"Added {e['xname']} to releases")
            except ValueError:
                pass

        try:
            next_page = response.headers["X-Next-Page"]
            if next_page.strip() == "":
                next_page = None
        except ValueError:
            next_page = None

    release_envs.sort(
        key=lambda e: datetime.fromisoformat(e['created_at']),
        reverse=True,
    )
    develop_envs.sort(
        key=lambda e: datetime.fromisoformat(e['updated_at']),
        reverse=True,
    )

    try:
        os.mkdir(ns.output)
    except FileExistsError:
        pass
    out_path = ns.output / "index.html"
    print(f"Dumping into '{out_path}' ...")
    j2t.stream(develop_envs=develop_envs, release_envs=release_envs) \
            .dump(str(out_path))
    print("Done")


def main():
    parser = argparse.ArgumentParser(
        prog="gen-pages",
        description="Generates a page with links to documentation versions",
    )
    parser.add_argument(
        "-o", "--output",
        help="Output directory",
        type=Path,
        default=Path("public"),
    )
    parser.add_argument(
        "-g", "--gitlab",
        help="Root URL of the GitLab instance",
        default="https://gitlab.nic.cz"
    )
    parser.add_argument(
        "-t", "--token",
        help="GitLab REST API authentication token",
        required=True,
        type=str
    )
    parser.add_argument(
        "-p", "--project",
        help="GitLab project identifier",
        required=True,
        type=str,
    )

    ns = parser.parse_args()
    generate(ns)


if __name__ == "__main__":
    main()
